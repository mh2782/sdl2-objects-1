#ifndef PLAYER_H_
#define PLAYER_H_

#include "SDL2Common.h"
#include "Animation.h"

// Player Animation states
#define LEFT 0
#define RIGHT 1
#define UP 2
#define DOWN 3
#define IDLE 4

typedef struct Player 
{
    // Texture which stores the sprite sheet (this
    // will be optimised).
    SDL_Texture*     texture = nullptr;

    // Player properties
    SDL_Rect targetRectangle;

    float speed = 50.0f;
    float x = 250.0f;
    float y = 250.0f;
    float vx = 0.0f;
    float vy = 0.0f;

    // Sprite information
    const int SPRITE_HEIGHT = 64;
    const int SPRITE_WIDTH = 32;
    
    // Animation state
    int state;

    // Animations
    static const int MAX_ANIMATIONS = 5;
    Animation* animations[MAX_ANIMATIONS];
} Player;

void initPlayer(Player* player, SDL_Renderer *renderer);
void destPlayer(Player* player);
void drawPlayer(Player* player, SDL_Renderer *renderer);
void processInput(Player *player, const Uint8 *keyStates);
void updatePlayer(Player* player, float timeDeltaInSeconds);

#endif