#include "NPC.h"
#include "TextureUtils.h"
#include <stdexcept>

NPC::NPC() {
    speed = 50.0f;

    //Position
    x = 250.0f;
    y = 250.0f;

    //Velocity
    vx = 0.0f;
    vy = 0.0f;

    //set the default state to idle
    state = IDLE;

    //set texture pointer
    texture = nullptr;

    //Target is the same size as the source
    targetRectangle.w = SPRITE_WIDTH;
    targetRectangle.h = SPRITE_HEIGHT;

    //initialise our animation texture pointers to null
    for (int i = 0; i < MAX_ANIMATIONS; i++) {
        animations[i] = nullptr;
    }
}

void NPC::init(SDL_Renderer* renderer) {
    //Create player texture from file, optimised for renderer
    texture = createTextureFromFile(
        "assets/images/undeadking.png",
        renderer);

    if (texture == nullptr)
        throw std::runtime_error("File not found");

    //Allocate memory for the animation structure
    for (int i = 0; i < MAX_ANIMATIONS; i++) {
        animations[i] = new Animation();
    }

    //Setup the animation structure
    animations[LEFT]->init(3, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 1);
    animations[RIGHT]->init(3, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 2);
    animations[UP]->init(3, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 3);
    animations[DOWN]->init(3, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 0);
    animations[IDLE]->init(1, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 0);
}

NPC::~NPC() {
    //Clean up animations
    for (int i = 0; i < MAX_ANIMATIONS; i++) {

        //Clean up the animation structure allocated with new so use delete
        delete animations[i];
        animations[i] = nullptr;
    }

    //clean up
    SDL_DestroyTexture(texture);
    texture = nullptr;
}


void NPC::draw(SDL_Renderer* renderer)
{
    // Get current animation based on the state.
    Animation* current = this->animations[state];
    SDL_RenderCopy(renderer,
        texture,
        current->getCurrentFrame(),
        &targetRectangle);
}

/**
 * processInput
 *
 * Function to process inputs for the player structure
 * Note: Need to think about other forms of input!
 *
 * @param player Player structure processing the input
 * @param keyStates The keystates array.
 */
void NPC::processInput(const Uint8* keyStates)
{
    // Process Player Input

    //Input - keys/joysticks?
    float verticalInput = 0.0f;
    float horizontalInput = 0.0f;

    // If no keys are down player should not animate!
    state = IDLE;

    // This could be more complex, e.g. increasing the vertical 
    // input while the key is held down. 
    if (keyStates[SDL_SCANCODE_UP])
    {
        verticalInput = -1.0f;
        state = UP;
    }

    if (keyStates[SDL_SCANCODE_DOWN])
    {
        verticalInput = 1.0f;
        state = DOWN;
    }

    if (keyStates[SDL_SCANCODE_RIGHT])
    {
        horizontalInput = 1.0f;
        state = RIGHT;
    }

    if (keyStates[SDL_SCANCODE_LEFT])
    {
        horizontalInput = -1.0f;
        state = LEFT;
    }

    // Calculate player velocity. 
    // Note: This is imperfect, no account taken of diagonal!
    vy = verticalInput * speed;
    vx = horizontalInput * speed;
}

/**
 * update
 *
 * Function to update the player structure and its components
 *
 * @param player Player structure being updated
 * @param timeDeltaInSeconds the time delta in seconds
 */
void NPC::update(float timeDeltaInSeconds)
{
    // Patrol
    if (x > 600.0f)
        vx = -200.0f;
    else
    {
        if (x < 400)
            vx = 200.0f;
    }
    //Update animation state
    if (vx > 1.0f)
        state = RIGHT;
    else
        state = LEFT;

    // Calculate distance travelled since last update
    float yMovement = timeDeltaInSeconds * vy;
    float xMovement = timeDeltaInSeconds * vx;

    // Update player position.
    x += xMovement;
    y += yMovement;

    // Move sprite to nearest pixel location.
    targetRectangle.y = round(y);
    targetRectangle.x = round(x);

    // Get current animation
    Animation* current = animations[state];

    // let animation update itself. 
    current->update(timeDeltaInSeconds);
}